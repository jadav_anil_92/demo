//
//  ViewController.swift
//  UpworkDemo
//
//  Created by MyMAC on 02/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var tableViewCard: UITableView!
    var data: [[String: Any]] = AppData.getCardData()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Sample Text"
        setupTableView()
        setupPlusButton()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setupTableView() {
        tableViewCard.delegate = self
        tableViewCard.dataSource = self
        tableViewCard.rowHeight = UITableView.automaticDimension;
        tableViewCard.estimatedRowHeight = 134.0;
        tableViewCard.tableFooterView = UIView()
    }
    
    func setupPlusButton() {
        let rightButton = UIBarButtonItem(image: UIImage(named: "plus_Icon"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.plusButtonAction))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func plusButtonAction() {
        print("plusButtonAction")
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! CardCell
        let dict = data[indexPath.row]
        cell.layoutIfNeeded()
        cell.containerView.setDropShadow()
        cell.lblTitle.text = dict["title"] as? String
        cell.lblAuthor.text = dict["author"] as? String
        cell.lblDate.text = AppData.getStringDate(date: dict["date"] as! Date)
        cell.indicatorView.backgroundColor = dict["color"] as? UIColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


