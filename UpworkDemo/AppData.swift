//
//  AppData.swift
//  SketchTest
//
//  Created by MyMAC on 02/04/19.
//  Copyright © 2019 Anil. All rights reserved.
//

import UIKit

class AppData: NSObject {
    class func getCardData() -> [[String: Any]]{
        let data = [["title":"Some bold title of card",
                     "author":"Author Name",
                     "color":UIColor(red: 145.0/255.0, green: 56.0/255.0, blue: 98.0/255.0, alpha: 1.0),
                     "date":Date()],
                    ["title":"Another card title",
                     "author":"Author Name",
                     "color":UIColor(red: 57.0/255.0, green: 161.0/255.0, blue: 66.0/255.0, alpha: 1.0),
                     "date":Date()],
                    ["title":"Another title. It’s long long title.Two lines title",
                     "author":"Author Name",
                     "color":UIColor(red: 39.0/255.0, green: 59.0/255.0, blue: 120.0/255.0, alpha: 1.0),
                     "date":Date()]] as [[String: Any]]
        return data
    }
    
    class func getStringDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd MMM yyyy • HH:mm"
        let myStringafd = formatter.string(from: yourDate!)
        return myStringafd
    }
}

extension UIView {
    func setDropShadow() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 7
        self.layer.borderWidth = 0.5
        self.layer.borderColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0).cgColor
        self.layer.shadowColor = UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1.0).cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.0
    }
}
